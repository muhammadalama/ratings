//
//  Player.swift
//  Ratings
//
//  Created by Muhammad Alam Akbar on 12/29/16.
//  Copyright © 2016 Muhammad Alam Akbar. All rights reserved.
//

import Foundation

struct Player {
    var name: String?
    var game: String?
    var rating: Int
    
    init(name: String? = "", game: String? = "", rating: Int) {
        self.name = name
        self.game = game
        self.rating = rating
    }
}
