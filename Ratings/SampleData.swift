//
//  SampleData.swift
//  Ratings
//
//  Created by Muhammad Alam Akbar on 12/29/16.
//  Copyright © 2016 Muhammad Alam Akbar. All rights reserved.
//

import Foundation

let playersData = [
    Player(name:"Bill Evans", game:"Tic-Tac-Toe", rating: 4),
    Player(name: "Oscar Peterson", game: "Spin the Bottle", rating: 5),
    Player(name: "Dave Brubeck", game: "Texas Hold 'em Poker", rating: 2)
]
