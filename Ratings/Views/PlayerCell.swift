//
//  PlayerTableViewCell.swift
//  Ratings
//
//  Created by Muhammad Alam Akbar on 12/30/16.
//  Copyright © 2016 Muhammad Alam Akbar. All rights reserved.
//

import UIKit

class PlayerCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var gameLabel: UILabel!
    @IBOutlet weak var ratingImageView: UIImageView!
    
    var player: Player? {
        didSet {
            if let player = player {
                nameLabel.text = player.name
                gameLabel.text = player.game
                ratingImageView.image = imageForRating(player.rating)
            }
        }
    }
    
    func imageForRating(_ rating: Int) -> UIImage? {
        let imageName = "\(rating)Stars"
        return UIImage(named: imageName)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
