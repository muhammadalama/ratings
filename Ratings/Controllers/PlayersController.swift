//
//  PlayersController.swift
//  Ratings
//
//  Created by Muhammad Alam Akbar on 12/30/16.
//  Copyright © 2016 Muhammad Alam Akbar. All rights reserved.
//

import UIKit

class PlayersController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var players: [Player] = playersData
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName:"PlayerCell", bundle:nil), forCellReuseIdentifier: "PlayerCell")
        tableView.register(UINib(nibName:"CustomCell", bundle:nil), forCellReuseIdentifier: "CustomCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelToPlayersViewController(segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func savePlayerDetail(segue: UIStoryboardSegue) {
        print("savePlayerDetail")
        if let playerDetailsController = segue.source as? PlayerDetailsController {
            print("savePlayerDetail 1")
            // add the new player to the players array
            if let player = playerDetailsController.player {
                players.append(player)
                
                // update the table view
                let indexPath = NSIndexPath(row: players.count-1, section: 0) as IndexPath
                tableView.insertRows(at: [indexPath], with: .automatic)
                
                print("savePlayerDetail 2")
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PlayersController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return players.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == -1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerCell", for: indexPath) as! PlayerCell
            let player = players[indexPath.row] as Player
            cell.player = player
            return cell
        } else {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as! CustomCell
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerCell", for: indexPath) as! PlayerCell
                let player = players[indexPath.row] as Player
                cell.player = player
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        tableView.reloadData()
    }
}
