//
//  ProfileViewController.swift
//  Ratings
//
//  Created by Muhammad Alam Akbar on 12/29/16.
//  Copyright © 2016 Muhammad Alam Akbar. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var labelNama: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    
    var name = "Asep"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelNama.text = "Asep"
        labelEmail.text = labelNama.text
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    // String getFullName() {
    //    return "Asep"
    // }
    func getFullName() -> String {
        let namaLengkap = "Udin"
        return namaLengkap
    }
    
    
}
