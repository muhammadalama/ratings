//
//  PageOneController.swift
//  Ratings
//
//  Created by Muhammad Alam Akbar on 1/3/17.
//  Copyright © 2017 Muhammad Alam Akbar. All rights reserved.
//

import UIKit

class PageOneController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var titleTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func pushButtonPressed(_ sender: Any) {
        let controller = PageTwoController.instantiate()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func modalButtonPressed(_ sender: Any) {
        let nav = PageTwoController.instantiateNav(name: titleTextField.text!)
        let controller = nav.topViewController as! PageTwoController
        //controller.titleLabel.text = titleTextField.text
        controller.delegate = self
        present(nav, animated: true, completion: nil)
    }
}

extension PageOneController: PageTwoControllerDelegate {
    
    func pageTwoControllerDidSubmit(controller: PageTwoController) {
        nameLabel.text = "Hello, \(controller.name). You write \(controller.titleIn)."
    }
    
}
