//
//  PageTwoController.swift
//  Ratings
//
//  Created by Muhammad Alam Akbar on 1/3/17.
//  Copyright © 2017 Muhammad Alam Akbar. All rights reserved.
//

import UIKit

protocol PageTwoControllerDelegate {
    func pageTwoControllerDidSubmit(controller: PageTwoController)
}

class PageTwoController: UIViewController {

    var delegate: PageTwoControllerDelegate?
    
    var name = "namamu"
    var titleIn = ""
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleTextField: UITextField!
    
    static func instantiate() -> PageTwoController {
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PageTwo") as! PageTwoController
        return controller
    }
    
    static func instantiateNav(name: String = "namamu") -> UINavigationController {
        let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PageTwoNav") as! UINavigationController
        let controller = nav.topViewController as! PageTwoController
        controller.name = name
        return nav;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        titleLabel.text = "Hello, \(name)!"
    }
    
    @IBAction func okButtonPressed(_ sender: Any) {
        titleIn = titleTextField.text!
        
        delegate?.pageTwoControllerDidSubmit(controller: self)
        dismiss(animated: true, completion: nil)
    }
}
